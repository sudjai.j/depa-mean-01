const async = require('async');

const step1 = function (callback) {
  console.log('step1');
  callback(null, 'one', 'two');
};

const step2 = function (arg1, arg2, callback) {
  console.log('step2');
  // arg1 = 'one','two'
  callback(null, 'three');
};

const final = function (err, result) {
  if (err) {
    console.log(err);
  } else {
    console.log(result);
  }
};

async.waterfall([step1, step2], final);
