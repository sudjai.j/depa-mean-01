var express = require('express');
var router = express.Router();
const pool = require('./configMySQL');
const smtpTransport = require('./nodeMailer');
const excel = require('excel4node');
const { forEach } = require('async');

router.get('/', function (req, res) {
  const user = req.query;
  console.log(req.query);
  let sql = `SELECT id,code,name FROM user`;
  if (user.code && user.name) {
    console.log('in');
    sql =
      sql +
      ` WHERE code LIKE('%${user.code}%') AND name LIKE('%${user.name}%')`;
  }
  console.log(sql);
  pool.getConnection(function (err, connection) {
    if (err) {
      res.status(500).end(`error can not connect database`);
      console.log(err);
    } else {
      connection.query(sql, function (err, result) {
        connection.release();
        if (err) {
          res.status(400).end('sql error');
        } else {
          res.json(result);
          console.log(result);
        }
      });
    }
  });
  //   res.end('get user');
});

router.post('/', function (req, res) {
  let user = req.body;
  let sql = `insert into user(email,code,name,pwd) values('${user.email}','${user.code}','${user.name}','${user.pwd}')`;
  pool.getConnection(function (err, connection) {
    if (err) {
      res.status(500).end(`error can not connect database`);
      console.log(err);
    } else {
      connection.query(sql, function (err, result) {
        connection.release();
        if (err) {
          res.status(400).end(`sql error`);
          console.log(err);
        } else {
          //   res.status(201).send(`insert success`);
          console.log(result);
          const objMail = {
            to: user.email,
            subject: 'Welcome to new member',
            html: '<strong>Hi new member!</strong>',
          };
          smtpTransport.sendMail(objMail, function (err, success) {
            smtpTransport.close();
            if (err) {
              res.status(500).end('email eror');
              console.log(err);
            } else {
              res.status(201).end('create user & send email success');
              console.log(success);
            }
          });
        }
      });
    }
  });
  //   res.end('create user');
});

router.put('/:userId', function (req, res) {
  const userId = req.params.userId;
  const user = req.body;
  const sql = `UPDATE user SET code="${user.code}",name="${user.name}" WHERE id="${userId}"`;
  pool.getConnection(function (err, connection) {
    if (err) {
      res.status(500).end(`error can not connect database`);
      console.log(err);
    } else {
      connection.query(sql, function (err, result) {
        connection.release();
        if (err) {
          res.status(400).end(`sql error`);
          console.log(err);
        } else {
          res.status(200).end(`update success`);
          console.log(result);
        }
      });
    }
  });
  //   res.end('update user');
});

router.delete('/:userId', function (req, res) {
  const id = req.params.userId;
  const sql = `DELETE FROM user WHERE id="${id}"`;
  pool.getConnection(function (err, connection) {
    if (err) {
      res.status(500).end(`error can not connect database`);
      console.log(err);
    } else {
      connection.query(sql, function (err, result) {
        connection.release();
        if (err) {
          res.status(400).end(`sql error`);
          console.log(err);
        } else {
          res.status(204).send(`delete success`);
          console.log(result);
        }
      });
    }
  });
  //   res.end('delete user');
});

router.get('/export', function (req, res) {
  const user = req.query;
  console.log(req.query);
  let sql = `SELECT id,code,name,email FROM user`;
  if (user.code && user.name) {
    console.log('in');
    sql =
      sql +
      ` WHERE code LIKE('%${user.code}%') AND name LIKE('%${user.name}%')`;
  }
  console.log(sql);
  pool.getConnection(function (err, connection) {
    if (err) {
      res.status(500).end(`error can not connect database`);
      console.log(err);
    } else {
      connection.query(sql, function (err, result) {
        connection.release();
        if (err) {
          res.status(400).end('sql error');
        } else {
          console.log(result);
          const wb = new excel.Workbook();
          const ws = wb.addWorksheet('sheet 1');
          ws.cell(1, 1).string(`ลำดับ`);
          ws.cell(1, 2).string(`รหัส`);
          ws.cell(1, 3).string(`username`);
          ws.cell(1, 4).string(`email`);
          result.forEach((elem, index, arr) => {
            if (index !== 0) {
              ws.cell(index + 1, 1).number(index + 1);
              ws.cell(index + 1, 2).string(elem.code);
              ws.cell(index + 1, 3).string(elem.name);
              ws.cell(index + 1, 4).string(elem.email);
            }
          });
          // const wb = new excel.Workbook();
          // const ws = wb.addWorksheet('sheet 1');
          // ws.cell(1, 1).string('Hello World');
          wb.write('user.xlsx', res);
          // res.end('export done');
        }
      });
    }
  });
  //   res.end('get user');
});

module.exports = router;

// anonymous object
