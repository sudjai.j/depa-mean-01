const async = require('async');

const opt1 = function (callback) {
  setTimeout(function () {
    console.log('one');
    callback(null, 'one');
  }, 200);
};
const opt2 = function (callback) {
  setTimeout(function () {
    console.log('two');
    callback(null, 'two');
  }, 100);
};

const final = function (err, results) {
  console.dir(results);
};

async.parallel([opt1, opt2], final);
