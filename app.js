// var http = require('http');
var response = require('./handleResponse');
const user = require('./user');
// const bodyParser = require('body-parser');

// http
//   .createServer(function (req, res) {
//     res.writeHead(200, { 'Content-Type': 'text/plain' });
//     res.end('yes you can\n');
//   })
//   .listen(8000);
// console.log('server running port 8000');

// const handleResponse = function (req, res) {
//   res.writeHead(200, { 'Content-Type': 'text/plain' });
//   res.end('yes you can\n');
// };

// const server = http.createServer().listen(8000);
// server.on('request', response);
// server.on('request', handleResponse);

// console.log('server running port 8000');

var express = require('express');
var app = express();

// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(bodyParser.json({}));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.get('/', response);

// app.get('/demo', function (req, res) {
//   res.render('index', {
//     title: 'hello',
//     message: 'my word',
//   });
// });

app.use('/user', user);

// app.get('/ab?cd', function (req, res) {
//   res.end('this is abcd route');
// });

app.get('/ab+cd', function (req, res) {
  res.end('this is ab+cd route');
});

app.get('/users/:userId/books/:bookId', function (req, res) {
  let userId = req.params.userId;
  let bookId = req.params.bookId;
  res.end(`userId : ${userId}`);
});

var server = require('http').Server(app);
server.listen(8000);
app.use(express.static('web'));
console.log('server runing port 8000');
