const async = require('async');

const step1 = function (callback) {
  // do some stuff ...
  callback(null, 'one');
};
const step2 = function (callback) {
  callback(null, 'two');
};

const final = function (err, results) {
  // results is now equal to ["one","two"]
  if (err) {
    console.log(err);
  } else {
    console.log(results);
  }
};

async.series([step1, step2], final);
