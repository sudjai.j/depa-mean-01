const mysql = require('mysql');

const pool = mysql.createPool({
  host: 'you host name',
  user: 'you username',
  password: 'you password',
  database: 'you db',
  connectionLimit: 10,
  multipleStatements: true,
});

module.exports = pool;
