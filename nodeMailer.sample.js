const mailer = require('nodemailer');

const config = {
  host: 'you smtp server',
  port: 'you port',
  secure: 'is secure?',
  auth: {
    user: 'you email',
    pass: 'you password',
  },
};

const smtpTransport = mailer.createTransport(config);
module.exports = smtpTransport;
